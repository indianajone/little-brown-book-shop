const plugins = {
  autoprefixer: {},
  tailwindcss: './tailwind.js',
  'postcss-nested': {},
};

if (process.env.NODE_ENV === 'production') {
  plugins['@fullhuman/postcss-purgecss'] = {
    content: [
      './src/**/*.html',
      './src/**/*.vue',
    ],
  };
}

module.exports = {
  plugins,
};
