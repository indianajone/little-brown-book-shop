import client from '@/libs/api-client';

describe('Api client', () => {
  it('should be exist.', () => {
    expect(client).toBeDefined();
  });

  it('should have base url defined', () => {
    expect(client.defaults.baseURL).toBeDefined();
  });

  it('should have content-type as application/json by default', () => {
    expect(client.defaults.headers['Content-Type']).toBe('application/json');
  });
});
