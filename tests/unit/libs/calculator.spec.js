import calculator from '@/libs/calculator';

const mockHarryPotterBooks = [
  { title: 'Harry Potter 1', price: 100 },
  { title: 'Harry Potter 2', price: 100 },
  { title: 'Harry Potter 3', price: 100 },
  { title: 'Harry Potter 4', price: 100 },
  { title: 'Harry Potter 5', price: 100 },
  { title: 'Harry Potter 6', price: 100 },
  { title: 'Harry Potter 7', price: 100 },
];

describe('Calculator', () => {
  it('should sum total price.', () => {
    const total = calculator.sum([10, 20, 30, 40]);
    expect(total).toBe(100);
  });

  it('should not discount when buy only one Harry Potter book.', () => {
    const books = mockHarryPotterBooks.slice(0, 1);
    const discount = calculator.discount(books);

    expect(discount).toBe(0);
  });

  it('should not discount when buy one Harry Potter book and other book too.', () => {
    const books = [
      ...mockHarryPotterBooks.slice(0, 1),
      { title: 'Some other book', price: 100 },
    ];

    const discount = calculator.discount(books);

    expect(discount).toBe(0);
  });

  it('should calculate discount 10% for 2 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks.slice(0, 2);
    const discount = calculator.discount(books);

    expect(discount).toBe(20);
  });

  it('should calculate discount 10% for 2 unique Harry Potter books only even buy the other book too.', () => {
    const books = [
      ...mockHarryPotterBooks.slice(0, 2),
      { title: 'Some other book', price: 100 },
    ];
    const discount = calculator.discount(books);

    expect(discount).toBe(20);
  });

  it('should calculate discount 10% for 2 unique Harry Potter books only even buy 2 of each.', () => {
    const books = mockHarryPotterBooks.slice(0, 2);
    const discount = calculator.discount(books);

    expect(discount).toBe(20);
  });

  it('should calculate discount 11% for 3 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks.slice(0, 3);
    const discount = calculator.discount(books);

    expect(discount).toBe(33);
  });

  it('should calculate discount 12% for 4 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks.slice(0, 4);
    const discount = calculator.discount(books);

    expect(discount).toBe(48);
  });

  it('should calculate discount 13% for 5 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks.slice(0, 5);
    const discount = calculator.discount(books);

    expect(discount).toBe(65);
  });

  it('should calculate discount 14% for 6 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks.slice(0, 6);
    const discount = calculator.discount(books);

    expect(discount).toBeCloseTo(84);
  });

  it('should calculate discount 15% for 7 unique Harry Potter books.', () => {
    const books = mockHarryPotterBooks;
    const discount = calculator.discount(books);

    expect(discount).toBe(105);
  });

  it('should calculate discount 15% even buy more than 7 unique Harry Potter books.', () => {
    const books = [
      ...mockHarryPotterBooks,
      { title: 'Harry Potter 8', price: 100 },
    ];
    const discount = calculator.discount(books);

    expect(discount).toBe(120);
  });
});
