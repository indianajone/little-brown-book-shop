import client from '@/libs/api-client';
import books from '@/api/books';

jest.mock('@/libs/api-client', () => ({
  get: jest.fn(),
}));

describe('Books api', () => {
  it('should be exist.', () => {
    expect(books).toBeDefined();
  });

  it('should return all books.', async () => {
    const mockResponse = {
      data: [
        { title: 'Harry Potter' },
        { title: 'The Ocean Book' },
      ],
    };

    client.get.mockResolvedValueOnce(mockResponse);

    await expect(books.all()).resolves.toBe(mockResponse);
  });

  it('should throw an error when api fail to calls all books.', async () => {
    const errorMessage = 'fail to get all books';

    client.get.mockRejectedValueOnce(new Error(errorMessage));

    await expect(books.all()).rejects.toThrowError(errorMessage);
  });
});
