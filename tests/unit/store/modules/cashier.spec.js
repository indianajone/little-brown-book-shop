import { PAY } from '@/store/action-types';
import { MAKE_PAYMENT } from '@/store/mutation-types';
import cashier from '@/store/modules/cashier';

const { actions, mutations, state } = cashier;

describe('Cart actions', () => {
  it(`should commit to ${MAKE_PAYMENT} when action ${PAY} is occur`, () => {
    const commit = jest.fn();
    const currentState = state;
    const payment = { amount: 100, price: 90 };
    actions[PAY]({ commit, currentState }, payment);

    expect(commit).toHaveBeenCalledWith(MAKE_PAYMENT, payment);
  });
});

describe('Cart mutations', () => {
  it('should calculate change and update to state.', () => {
    const currentState = state;
    const payment = { amount: 100, price: 90 };

    mutations[MAKE_PAYMENT](currentState, payment);

    expect(currentState.amount).toBe(payment.amount);
    expect(currentState.price).toBe(payment.price);
    expect(currentState.change).toBe(10);
  });
});
