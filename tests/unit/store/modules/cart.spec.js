import {
  ADD_ITEM,
  EMPTY_CART,
  REMOVE_ITEM,
} from '@/store/action-types';
import {
  ADD_TO_CART,
  DECREMENT_QUANTITY,
  INCREMENT_QUANTITY,
  REMOVE_FROM_CART,
  RESET_CART,
  UPDATE_TOTAL,
} from '@/store/mutation-types';

import cart from '@/store/modules/cart';

const { actions, mutations, state } = cart;
const mockBook = { id: 1, title: 'Harry Potter', price: 100 };

describe('Cart actions', () => {
  let commit;
  let currentState;

  beforeEach(() => {
    commit = jest.fn();
    currentState = state;
  });

  it(`should commit to ${ADD_TO_CART} when item does not exist in the cart then commit ${UPDATE_TOTAL}.`, () => {
    actions[ADD_ITEM]({ commit, state }, mockBook);

    expect(commit).toHaveBeenCalledWith(ADD_TO_CART, { ...mockBook, quantity: 1 });
    expect(commit).toHaveBeenCalledWith(UPDATE_TOTAL);
    expect(commit).toBeCalledTimes(2);
  });

  it(`should commit to ${INCREMENT_QUANTITY} when same item was added to the cart then commit ${UPDATE_TOTAL}.`, () => {
    currentState = {
      ...state, items: [{ ...mockBook, quantity: 1 }], total: mockBook.price,
    };

    actions[ADD_ITEM]({ commit, state: currentState }, mockBook);

    expect(commit).toHaveBeenCalledWith(INCREMENT_QUANTITY, { ...currentState.items[0] });
    expect(commit).toHaveBeenCalledWith(UPDATE_TOTAL);
    expect(commit).toBeCalledTimes(2);
  });

  it('should not commit from the cart when item does not exist in the cart.', () => {
    actions[REMOVE_ITEM]({ commit, state }, mockBook.id);
    expect(commit).not.toBeCalled();
  });

  it(`should commit to ${REMOVE_FROM_CART} from the cart then commit ${UPDATE_TOTAL}.`, () => {
    currentState = {
      ...state, items: [{ ...mockBook, quantity: 1 }], total: mockBook.price,
    };

    actions[REMOVE_ITEM]({ commit, state: currentState }, mockBook.id);

    expect(commit).toHaveBeenCalledWith(REMOVE_FROM_CART, { ...currentState.items[0] });
    expect(commit).toHaveBeenCalledWith(UPDATE_TOTAL);
    expect(commit).toBeCalledTimes(2);
  });

  it(`should commit to ${DECREMENT_QUANTITY} from the cart then commit ${UPDATE_TOTAL}.`, () => {
    currentState = {
      ...state, items: [{ ...mockBook, quantity: 2 }], total: mockBook.price,
    };

    actions[REMOVE_ITEM]({ commit, state: currentState }, mockBook.id);

    expect(commit).toHaveBeenCalledWith(DECREMENT_QUANTITY, { ...currentState.items[0] });
    expect(commit).toHaveBeenCalledWith(UPDATE_TOTAL);
    expect(commit).toBeCalledTimes(2);
  });

  it(`should commit to ${RESET_CART}`, () => {
    currentState = {
      ...state, items: [{ ...mockBook, quantity: 1 }], total: mockBook.price,
    };

    actions[EMPTY_CART]({ commit, state: currentState });

    expect(commit).toHaveBeenCalledWith(RESET_CART);
    expect(commit).toBeCalledTimes(1);
  });
});

describe('Cart mutations', () => {
  it('should add item to the cart.', () => {
    const currentState = { items: [] };
    mutations[ADD_TO_CART](currentState, { ...mockBook, quantity: 1 });

    expect(currentState.items).toHaveLength(1);
    expect(currentState.items[0].title).toBe(mockBook.title);
  });

  it('should increment quantity from given item in the cart.', () => {
    const currentState = { items: [{ ...mockBook, quantity: 1 }] };
    mutations[INCREMENT_QUANTITY](currentState, currentState.items[0]);

    expect(currentState.items).toHaveLength(1);
    expect(currentState.items[0].title).toBe(mockBook.title);
    expect(currentState.items[0].quantity).toBe(2);
  });

  it('should decrement quantity from given item in the cart.', () => {
    const currentState = { items: [{ ...mockBook, quantity: 2 }] };
    mutations[DECREMENT_QUANTITY](currentState, currentState.items[0]);

    expect(currentState.items).toHaveLength(1);
    expect(currentState.items[0].title).toBe(mockBook.title);
    expect(currentState.items[0].quantity).toBe(1);
  });

  it('should remove item to the cart', () => {
    const currentState = {
      items: [
        { ...mockBook, quantity: 1 },
        { ...mockBook, id: 2, quantity: 2 },
      ],
    };
    mutations[REMOVE_FROM_CART](currentState, currentState.items[0]);

    expect(currentState.items).toHaveLength(1);
    expect(currentState.items[0].id).toBe(2);
  });

  it('should update total price.', () => {
    const currentState = {
      items: [
        { ...mockBook, quantity: 1, title: 'foo' },
        { ...mockBook, id: 2, quantity: 2 },
      ],
      total: 0,
    };

    mutations[UPDATE_TOTAL](currentState);

    expect(currentState.total).toBe(300);
  });

  it('should update total price with discount if buy 2 of unique Harry Potter books.', () => {
    const currentState = {
      items: [
        { ...mockBook, quantity: 1 },
        {
          id: 2, quantity: 2, title: 'Harry Potter 2', price: 100,
        },
      ],
      total: 0,
    };

    mutations[UPDATE_TOTAL](currentState);

    expect(currentState.total).toBe(280);
  });

  it('should reset state to it initial state.', () => {
    const currentState = {
      discount: 0,
      items: [
        { ...mockBook, quantity: 1, title: 'foo' },
        { ...mockBook, id: 2, quantity: 2 },
      ],
      total: 300,
    };

    mutations[RESET_CART](currentState);

    expect(currentState.discount).toBe(0);
    expect(currentState.items).toHaveLength(0);
    expect(currentState.total).toBe(0);
  });
});
