import api from '@/api/books';
import { GET_ALL_BOOKS } from '@/store/action-types';
import { UPDATE_BOOK_STORE, UPDATE_BOOK_STORE_ERROR } from '@/store/mutation-types';

import books from '@/store/modules/books';

jest.mock('@/api/books', () => ({
  all: jest.fn(),
}));

const { actions, mutations } = books;

const mockBooks = [
  { id: 1, title: 'Harry Potter' },
  { id: 2, title: 'The Ocean Book' },
];

describe('Books actions', () => {
  it('should get all books from api.', async () => {
    const commit = jest.fn();
    const mockData = {
      books: mockBooks,
    };
    const mockResponse = { data: mockData };

    api.all.mockResolvedValueOnce(mockResponse);

    await actions[GET_ALL_BOOKS]({ commit });
    expect(commit).toBeCalledTimes(1);
    expect(commit).toHaveBeenCalledWith(UPDATE_BOOK_STORE, mockData.books);
  });

  it('should dispath an error when get all api failed.', async () => {
    const commit = jest.fn();
    const error = new Error('fail to get all books');

    api.all.mockRejectedValueOnce(error);

    await actions[GET_ALL_BOOKS]({ commit });

    expect(commit).toBeCalledTimes(1);
    expect(commit).toHaveBeenCalledWith(UPDATE_BOOK_STORE_ERROR, error.message);
  });
});

describe('Books mutations', () => {
  it('should update books.', () => {
    const state = { all: [] };

    mutations[UPDATE_BOOK_STORE](state, mockBooks);

    expect(state.all).toHaveLength(2);
    expect(state.all[0]).toBe(mockBooks[0]);
    expect(state.all[1]).toBe(mockBooks[1]);
  });

  it('should update books error.', () => {
    const error = new Error('fail to get all books');
    const state = { error: '' };

    mutations[UPDATE_BOOK_STORE_ERROR](state, error.message);

    expect(state.error).toBe(error.message);
  });
});
