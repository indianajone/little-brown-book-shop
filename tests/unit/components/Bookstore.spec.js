import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import Bookstore from '@/components/Bookstore.vue';
import { GET_ALL_BOOKS } from '@/store/action-types';
import books from '@/store/modules/books';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Bookstore.vue', () => {
  let actions;
  let store;

  beforeEach(() => {
    actions = {
      [GET_ALL_BOOKS]: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        books: Object.assign({}, books, { actions }),
      },
    });
  });

  it('should get all books once component is created.', () => {
    shallowMount(Bookstore, { store, localVue });

    expect(actions[GET_ALL_BOOKS].mock.calls).toHaveLength(1);
  });
});
