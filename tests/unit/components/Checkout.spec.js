import Vuex from 'vuex';
import VModal from 'vue-js-modal';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import Checkout from '@/components/Checkout.vue';
import cart from '@/store/modules/cart';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VModal);

describe('Checkout.vue', () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        cart,
      },
    });
  });

  it('should not be able to pay when the cart is empty.', () => {
    const wrapper = shallowMount(Checkout, {
      store,
      localVue,
    });

    wrapper.vm.$modal.show = jest.fn();
    wrapper.find('#checkout-button').trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(0);
  });

  it('should show payment modal when user clicked Pay and there is at least one item in the cart.', () => {
    const wrapper = shallowMount(Checkout, {
      store,
      localVue,
      computed: {
        total: () => 100,
      },
    });

    wrapper.vm.$modal.show = jest.fn();
    wrapper.find('#checkout-button').trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls[0][0].name).toBe('Payment');
    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(1);
  });
});
