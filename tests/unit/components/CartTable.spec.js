import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import VueCurrencyFilter from 'vue-currency-filter';

import CartTable from '@/components/CartTable.vue';
import { REMOVE_ITEM } from '@/store/action-types';
import cart from '@/store/modules/cart';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueCurrencyFilter);

describe('CartTable.vue', () => {
  const item = {
    id: 1, title: 'Harry Potter', cover: 'some-image.jpg', price: 10, quantity: 1,
  };
  let actions;
  let store;

  beforeEach(() => {
    actions = {
      [REMOVE_ITEM]: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        cart: Object.assign({}, cart, { actions }),
      },
    });
  });

  it('should display empty message when cart is empty.', () => {
    const wrapper = shallowMount(CartTable, {
      localVue,
      store,
    });

    expect(wrapper.html()).toContain('Your cart is empty...');
  });

  it('should contain book info when book was added to the cart.', () => {
    const wrapper = shallowMount(CartTable, {
      localVue,
      store,
      computed: {
        items: () => [item],
      },
    });

    const html = wrapper.html();

    expect(html).toContain(item.title);
    expect(html).toContain(item.price);
  });

  it('should be able remove an existing item in a cart.', () => {
    const wrapper = shallowMount(CartTable, {
      localVue,
      store,
      computed: {
        items: () => [item],
      },
    });

    wrapper.find(`#remove-button-${item.id}`).trigger('click');

    expect(actions[REMOVE_ITEM].mock.calls).toHaveLength(1);
  });
});
