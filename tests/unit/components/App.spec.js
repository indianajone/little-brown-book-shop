import { shallowMount } from '@vue/test-utils';

import App from '@/components/App.vue';

describe('App.vue', () => {
  it('should have a title.', () => {
    const wrapper = shallowMount(App, {
      stubs: {
        'modals-container': true,
        notifications: true,
      },
    });

    expect(wrapper.find('h1').text()).toBe('Little Brown Book Shop');
  });
});
