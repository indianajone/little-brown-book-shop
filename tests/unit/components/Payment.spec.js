import Vuex from 'vuex';
import VModal from 'vue-js-modal';
import Notifications from 'vue-notification';
import VueCurrencyFilter from 'vue-currency-filter';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import Payment from '@/components/Payment.vue';
import cart from '@/store/modules/cart';
import cashier from '@/store/modules/cashier';
import { PAY } from '@/store/action-types';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VModal);
localVue.use(Notifications);
localVue.use(VueCurrencyFilter);

describe('Payment.vue', () => {
  let actions;
  let store;

  beforeEach(() => {
    actions = {
      [PAY]: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        cart,
        cashier: Object.assign({}, cashier, { actions }),
      },
    });
  });

  it('should not be able to pay when cash value has not been input', () => {
    const wrapper = shallowMount(Payment, {
      store,
      localVue,
    });

    wrapper.vm.$modal.show = jest.fn();
    wrapper.find('#payment-button').trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(0);
  });

  it('should not be able to pay when cash value is less than total price', () => {
    const wrapper = shallowMount(Payment, {
      store,
      localVue,
      computed: {
        total: () => 100,
      },
    });

    wrapper.setData({ cash: 20 });
    wrapper.vm.$modal.show = jest.fn();
    wrapper.find('#payment-button').trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(0);
  });

  it('should be able to pay once cash value is over total price', () => {
    const wrapper = shallowMount(Payment, {
      store,
      localVue,
      computed: {
        total: () => 98,
      },
    });
    const input = wrapper.find('#payment-cash');

    input.element.value = 100;
    input.trigger('input');

    wrapper.vm.$modal.show = jest.fn();
    wrapper.find('#payment-button').trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls[0][0].name).toBe('Receipt');
    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(1);
  });

  it('should display change when staff enter a valid cash', () => {
    const wrapper = shallowMount(Payment, {
      store,
      localVue,
      computed: {
        total: () => 98,
      },
    });
    const input = wrapper.find('#payment-cash');

    input.element.value = 100;
    input.trigger('input');

    expect(wrapper.vm.isPayable).toBeTruthy();
    expect(wrapper.vm.change).toBe(2);
  });
});
