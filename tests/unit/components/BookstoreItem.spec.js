import Vuex from 'vuex';
import Notifications from 'vue-notification';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import BookstoreItem from '@/components/BookstoreItem.vue';
import { ADD_ITEM } from '@/store/action-types';
import cart from '@/store/modules/cart';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Notifications);

describe('BookstoreItem.vue', () => {
  const item = {
    id: 1, title: 'Harry Potter', cover: 'some-image.jpg', price: 10,
  };
  let actions;
  let store;

  beforeEach(() => {
    actions = {
      [ADD_ITEM]: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        cart: Object.assign({}, cart, { actions }),
      },
    });
  });

  it('should have book cover rendered.', () => {
    const wrapper = shallowMount(BookstoreItem, {
      propsData: { item },
    });
    const coverImage = wrapper.find('img');

    expect(coverImage.attributes('src')).toBe(item.cover);
    expect(coverImage.attributes('alt')).toBe(item.title);
  });

  it('should have book title rendered.', () => {
    const wrapper = shallowMount(BookstoreItem, {
      propsData: { item },
    });

    expect(wrapper.find('h3').text()).toBe(item.title);
  });

  it('should have book price rendered.', () => {
    const wrapper = shallowMount(BookstoreItem, {
      propsData: { item },
    });

    expect(wrapper.find('p').text()).toBe(`${item.price}`);
  });

  it(`should call ${ADD_ITEM} action when user clicked.`, () => {
    const wrapper = shallowMount(BookstoreItem, {
      store,
      localVue,
      propsData: { item },
    });

    wrapper.trigger('click');

    expect(actions[ADD_ITEM].mock.calls).toHaveLength(1);
  });
});
