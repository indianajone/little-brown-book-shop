import { shallowMount } from '@vue/test-utils';

import CloseButton from '@/components/CloseButton.vue';

describe('CloseButton.vue', () => {
  it('should passed prop function when clicked.', () => {
    const wrapper = shallowMount(CloseButton, {
      propsData: {
        onClose: jest.fn(),
      },
    });

    wrapper.trigger('click');

    expect(wrapper.vm.onClose).toBeCalledTimes(1);
  });
});
