import Vuex from 'vuex';
import VModal from 'vue-js-modal';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import Cart from '@/components/Cart.vue';
import cart from '@/store/modules/cart';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VModal);

describe('Cart.vue', () => {
  let store;

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        cart,
      },
    });
  });

  it('should display number of books in cart.', () => {
    const wrapper = shallowMount(Cart, { store, localVue });

    expect(wrapper.find('#cart-button-badge').text()).toBe('0');
  });

  it('should show checkout modal when user clicked.', () => {
    const wrapper = shallowMount(Cart, { store, localVue });

    wrapper.vm.$modal.show = jest.fn();
    wrapper.trigger('click');

    expect(wrapper.vm.$modal.show.mock.calls[0][0].name).toBe('Checkout');
    expect(wrapper.vm.$modal.show.mock.calls).toHaveLength(1);
  });
});
