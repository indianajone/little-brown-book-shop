import Vuex from 'vuex';
import VueCurrencyFilter from 'vue-currency-filter';
import { createLocalVue, shallowMount } from '@vue/test-utils';

import Receipt from '@/components/Receipt.vue';
import { EMPTY_CART, PAY } from '@/store/action-types';
import cart from '@/store/modules/cart';
import cashier from '@/store/modules/cashier';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueCurrencyFilter);

describe('Receipt.vue', () => {
  let cartActions;
  let cashierActions;
  let store;
  const item = {
    id: 1, title: 'Harry Potter', cover: 'some-image.jpg', price: 10, quantity: 1,
  };

  beforeEach(() => {
    cartActions = {
      [EMPTY_CART]: jest.fn(),
    };

    cashierActions = {
      [PAY]: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        cart: Object.assign({}, cart, { actions: cartActions }),
        cashier: Object.assign({}, cashier, { actions: cashierActions }),
      },
    });
  });

  it('should contain book info when book was added to the cart.', () => {
    const wrapper = shallowMount(Receipt, {
      localVue,
      store,
      computed: {
        amount: () => 100,
        change: () => 90,
        items: () => [item],
      },
    });

    const html = wrapper.html();

    expect(html).toContain(item.title);
    expect(html).toContain(item.price);
  });

  it('should empty the cart and $emit `close` event when done button was clicked.', () => {
    const wrapper = shallowMount(Receipt, {
      localVue,
      store,
    });

    wrapper.vm.$emit = jest.fn();
    wrapper.find('#receipt-done-button').trigger('click');

    expect(cartActions[EMPTY_CART].mock.calls).toHaveLength(1);
    expect(wrapper.vm.$emit).toHaveBeenCalledWith('close');
  });
});
