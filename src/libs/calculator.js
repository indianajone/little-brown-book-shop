/**
 * Map of discount lookup logic.
 */
const discountMap = {
  2: 10,
  3: 11,
  4: 12,
  5: 13,
  6: 14,
  7: 15,
};

/**
 * Sum given array of numbers.
 *
 * @param {{ title: string, price: number }[]} items
 * @returns {number} Total
 */
const sum = items => items.reduce((accumulator, value) => (accumulator + value), 0);

/**
 * Calculate discount base on discountMap logic.
 *
 * @param {{ title: string, price: number }[]} items
 * @returns {number} Total discount
 */
const discount = (items) => {
  const discountable = items.filter(({ title }) => title.includes('Harry Potter'));
  const total = sum(discountable.map(({ price }) => parseFloat(price)));

  if (discountable.length >= 2 && discountMap[discountable.length]) {
    return total * (discountMap[discountable.length] / 100);
  }

  if (discountable.length > 7) {
    return total * (discountMap[7] / 100);
  }

  return 0;
};

export default {
  discount,
  sum,
};
