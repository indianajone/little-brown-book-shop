import Vue from 'vue';
import Notifications from 'vue-notification';
import VModal from 'vue-js-modal';
import VueCurrencyFilter from 'vue-currency-filter';

import '@/assets/css/main.css';
import App from './components/App.vue';
import store from './store';

Vue.use(Notifications);
Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false } });
Vue.use(VueCurrencyFilter, {
  symbol: '',
  thousandsSeparator: ',',
  fractionCount: 2,
  fractionSeparator: '.',
});
Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
