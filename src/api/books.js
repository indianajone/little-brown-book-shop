import client from '@/libs/api-client';

const getAllBooks = () => client.get('/b/5c52a1be15735a25423d3540');

export default {
  all: getAllBooks,
};
