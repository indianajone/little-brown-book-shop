// Books
const GET_ALL_BOOKS = 'GET_ALL_BOOKS';

const BOOKS = {
  GET_ALL_BOOKS: `books/${GET_ALL_BOOKS}`,
};

// Cart
const ADD_ITEM = 'ADD_ITEM';
const REMOVE_ITEM = 'REMOVE_ITEM';
const EMPTY_CART = 'EMPTY_CART';

const CART = {
  ADD_ITEM: `cart/${ADD_ITEM}`,
  REMOVE_ITEM: `cart/${REMOVE_ITEM}`,
  EMPTY_CART: `cart/${EMPTY_CART}`,
};

// Cashier
const PAY = 'PAY';

const CASHIER = {
  PAY: `cashier/${PAY}`,
};

export {
  ADD_ITEM,
  BOOKS,
  CART,
  CASHIER,
  EMPTY_CART,
  GET_ALL_BOOKS,
  REMOVE_ITEM,
  PAY,
};
