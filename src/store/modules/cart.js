import {
  ADD_ITEM,
  EMPTY_CART,
  REMOVE_ITEM,
} from '@/store/action-types';
import {
  ADD_TO_CART,
  DECREMENT_QUANTITY,
  INCREMENT_QUANTITY,
  REMOVE_FROM_CART,
  RESET_CART,
  UPDATE_TOTAL,
} from '@/store/mutation-types';
import calculator from '@/libs/calculator';

const initialState = () => ({
  discount: 0,
  items: [],
  total: 0,
});

const actions = {
  [ADD_ITEM]({ commit, state }, item) {
    const prev = state.items.find(({ id }) => id === item.id);

    if (prev) {
      commit(INCREMENT_QUANTITY, prev);
    } else {
      commit(ADD_TO_CART, { ...item, quantity: 1 });
    }

    commit(UPDATE_TOTAL);
  },

  [EMPTY_CART]({ commit }) {
    commit(RESET_CART);
  },

  [REMOVE_ITEM]({ commit, state }, id) {
    const prev = state.items.find(item => item.id === id);
    if (prev) {
      if (prev.quantity > 1) {
        commit(DECREMENT_QUANTITY, prev);
      } else {
        commit(REMOVE_FROM_CART, prev);
      }
      commit(UPDATE_TOTAL);
    }
  },
};

const getters = {};

const mutations = {
  [ADD_TO_CART](state, item) {
    state.items.push(item);
  },

  [DECREMENT_QUANTITY](_state, item) {
    // eslint-disable-next-line no-param-reassign
    item.quantity -= 1;
  },

  [INCREMENT_QUANTITY](_state, item) {
    // eslint-disable-next-line no-param-reassign
    item.quantity += 1;
  },

  [REMOVE_FROM_CART](state, item) {
    const index = state.items.findIndex(({ id }) => id === item.id);
    state.items.splice(index, 1);
  },

  [RESET_CART](state) {
    Object.assign(state, initialState());
  },

  [UPDATE_TOTAL](state) {
    const discount = calculator.discount(state.items);
    const total = calculator.sum(state.items.map(({ quantity, price }) => price * quantity));
    /* eslint-disable no-param-reassign */
    state.discount = discount;
    state.total = total - discount;
    /* eslint-emable no-param-reassign */
  },
};

export default {
  actions,
  getters,
  mutations,
  namespaced: true,
  state: initialState(),
};
