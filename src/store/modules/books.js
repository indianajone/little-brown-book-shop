import books from '@/api/books';
import { GET_ALL_BOOKS } from '@/store/action-types';
import { UPDATE_BOOK_STORE, UPDATE_BOOK_STORE_ERROR } from '@/store/mutation-types';

const initialState = {
  all: [],
  error: '',
};

const actions = {
  async [GET_ALL_BOOKS]({ commit }) {
    try {
      const { data } = await books.all();
      commit(UPDATE_BOOK_STORE, data.books);
    } catch (error) {
      commit(UPDATE_BOOK_STORE_ERROR, error.message);
    }
  },
};

const getters = {};

const mutations = {
  [UPDATE_BOOK_STORE](state, data) {
    // eslint-disable-next-line no-param-reassign
    state.all = data;
  },
  [UPDATE_BOOK_STORE_ERROR](state, error) {
    // eslint-disable-next-line no-param-reassign
    state.error = error;
  },
};

export default {
  actions,
  getters,
  mutations,
  namespaced: true,
  state: initialState,
};
