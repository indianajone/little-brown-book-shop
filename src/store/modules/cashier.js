
import { PAY } from '@/store/action-types';
import { MAKE_PAYMENT } from '@/store/mutation-types';

const initialState = () => ({
  amount: 0,
  paymantType: 'cash',
  price: 0,
  change: 0,
});

const actions = {
  [PAY]({ commit }, { amount, price }) {
    commit(MAKE_PAYMENT, {
      amount,
      price,
    });
  },
};

const getters = {};

const mutations = {
  [MAKE_PAYMENT](state, payment) {
    Object.assign(state, {
      ...payment,
      change: (payment.amount - payment.price),
    });
  },
};

export default {
  actions,
  getters,
  mutations,
  namespaced: true,
  state: initialState(),
};
