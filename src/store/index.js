import Vue from 'vue';
import Vuex from 'vuex';

import books from './modules/books';
import cart from './modules/cart';
import cashier from './modules/cashier';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    books,
    cart,
    cashier,
  },
  strict: process.env.NODE_ENV !== 'production',
});
