module.exports = {
  theme: {
    extend: {
      colors: {
        'line-gray-dark': '#444444',
        'line-gray-light': '#A3A7AE',
        'line-green': '#00B900',
      },
    },
  },
  variants: {},
  plugins: [],
};
