# Little Brown Book Shop

This project is a simple poc version of `Little Brown Book Shop` (POS)

Checkout the [Live demo](https://indianajone.gitlab.io/little-brown-book-shop)

## Getting start

### Prerequisite

1. Please make sure to set your `ENV` according to your environment, use `.env.test` as example. For example if deploy to `beta` environment, run ```cp .env.test .env.beta``` then set all variables as required.

    > Note: api `endpoint` must be the same on each environment.

1. run `yarn install` to install all dependencies.

#### Run development mode (Local)

```
yarn run serve
```

#### Build for production

```
yarn run build
```

#### Linting

```
yarn run lint
```

#### Run unit tests

```
yarn run test:unit
```

## Room for improvement

Due to limited of time, this project was build to demonstrate the use of Vuejs Framework. Even Though project is production ready but there are always plenty of rooms that could be improved.

Here are some of things that could be add/improve to the project;
- Add search for book functinality
- Add paginations
- Handle error when unabled to get all books from api
